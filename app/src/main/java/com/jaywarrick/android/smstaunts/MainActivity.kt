package com.jaywarrick.android.smstaunts

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.os.Bundle
import android.provider.Telephony
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val hasReceiveSms = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED

        if (!hasReceiveSms) {
            requestPermissions(arrayOf(Manifest.permission.RECEIVE_SMS), 1)
        } else {
            Toast.makeText(this, "Your permissions are happy, text away", Toast.LENGTH_LONG).show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        Toast.makeText(this, "SMS permission is required!", Toast.LENGTH_LONG).show()
        finish()
    }
}

class SmsListener : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION == intent.action) {
            Telephony.Sms.Intents.getMessagesFromIntent(intent).forEach {
                val body = it.messageBody
                val number : Int? = body.toIntOrNull()
                if (number in 1..21) {
                    playNumber(context, number!!)
                }
            }
        }
    }

    private fun playNumber(context: Context, taunt: Int) {
        val player = MediaPlayer()
        player.setDataSource(context.assets.openFd("$taunt.ogg"))
        player.prepare()
        player.start()
    }
}