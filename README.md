# Sms Taunts

Taunts that play when you receive an SMS Message.

### Usage

* Install the app
* Open the app to grant a permission
* Now taunt sounds will play when receiving certain SMS messages

### Sounds supported

* 1-30: Taunts from Age of Empires II

### TODO

* Support more taunts
* Honor system silent. Ideally play only when a notification sound would
* Play sounds on outgoing messages too 
